import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        navigation: [
            {
                id: 1,
                text: 'Men',
                url: 'javascript:;',
                class: 'random-class',
                items: [
                    {
                        id: 11,
                        text: 'Clothing',
                        url: 'javascript:;',
                        items: [
                            {
                                id: 111,
                                text: 'Ski',
                                url: 'javascript:;'
                            },
                            {
                                id: 112,
                                text: 'Fleece Jackets',
                                url: 'javascript:;'
                            },
                            {
                                id: 113,
                                text: 'Insulated Jackets',
                                url: 'javascript:;'
                            },
                            {
                                id: 114,
                                text: 'Rain Jackets',
                                url: 'javascript:;'
                            },
                            {
                                id: 115,
                                text: 'Softshell Jackets',
                                url: 'javascript:;'
                            },
                            {
                                id: 116,
                                text: 'Vests',
                                url: 'javascript:;'
                            },
                            {
                                id: 117,
                                text: 'Pullovers',
                                url: 'javascript:;'
                            },
                            {
                                id: 118,
                                text: 'Baselayers',
                                url: 'javascript:;'
                            },
                            {
                                id: 119,
                                text: 'Shirts & Tops',
                                url: 'javascript:;'
                            },
                            {
                                id: 120,
                                text: 'Pants',
                                url: 'javascript:;'
                            },
                            {
                                id: 121,
                                text: 'Shorts',
                                url: 'javascript:;'
                            },
                            {
                                id: 122,
                                text: 'Shop All',
                                url: 'javascript:;'
                            }
                        ]
                    },
                    {
                        id: 12,
                        text: 'Footwear',
                        url: 'javascript:;',
                        items: [
                            {
                                id: 121,
                                text: 'Hiking',
                                url: 'javascript:;'
                            },
                            {
                                id: 122,
                                text: 'Winter',
                                url: 'javascript:;'
                            },
                            {
                                id: 123,
                                text: 'Casual',
                                url: 'javascript:;'
                            },
                            {
                                id: 124,
                                text: 'Sandals',
                                url: 'javascript:;'
                            },
                            {
                                id: 125,
                                text: 'Shop All',
                                url: 'javascript:;'
                            }
                        ]
                    },
                    {
                        id: 13,
                        text: 'Accessories',
                        url: 'javascript:;',
                        items: [
                            {
                                id: 131,
                                text: 'Backpacks & Bags',
                                url: 'javascript:;'
                            },
                            {
                                id: 132,
                                text: 'Headwear',
                                url: 'javascript:;'
                            },
                            {
                                id: 133,
                                text: 'Gloves',
                                url: 'javascript:;'
                            },
                            {
                                id: 134,
                                text: 'Shop All',
                                url: 'javascript:;'
                            }
                        ]
                    },
                    {
                        id: 14,
                        text: 'Shop to stay',
                        url: 'javascript:;',
                        items: [
                            {
                                id: 141,
                                text: 'Dry',
                                url: 'javascript:;'
                            },
                            {
                                id: 142,
                                text: 'Cool',
                                url: 'javascript:;'
                            },
                            {
                                id: 143,
                                text: 'Protected',
                                url: 'javascript:;'
                            },
                            {
                                id: 144,
                                text: 'Warm',
                                url: 'javascript:;'
                            }
                        ]
                    },
                    {
                        id: 15,
                        text: 'Shop by activity',
                        url: 'javascript:;',
                        items: [
                            {
                                id: 151,
                                text: 'Hiking / Trekking',
                                url: 'javascript:;'
                            },
                            {
                                id: 152,
                                text: 'Casual / Travel',
                                url: 'javascript:;'
                            },
                            {
                                id: 153,
                                text: 'Ski / Snow',
                                url: 'javascript:;'
                            },
                            {
                                id: 154,
                                text: 'Fishing',
                                url: 'javascript:;'
                            }
                        ]
                    }
                ]
            },
            {
                id: 2,
                text: 'Women',
                url: 'javascript:;',
                active: true,
                items: [
                    {
                        id: 14,
                        text: 'Shop to stay',
                        url: 'javascript:;',
                        items: [
                            {
                                id: 141,
                                text: 'Dry',
                                url: 'javascript:;'
                            },
                            {
                                id: 119,
                                text: 'Shirts & Tops',
                                url: 'javascript:;'
                            },
                            {
                                id: 120,
                                text: 'Pants',
                                url: 'javascript:;'
                            },
                            {
                                id: 121,
                                text: 'Shorts',
                                url: 'javascript:;'
                            },
                            {
                                id: 122,
                                text: 'Shop All',
                                url: 'javascript:;'
                            }
                        ]
                    },
                    {
                        id: 11,
                        text: 'Clothing',
                        url: 'javascript:;',
                    },
                    {
                        id: 15,
                        text: 'Shop by activity',
                        url: 'javascript:;',
                        items: [
                            {
                                id: 151,
                                text: 'Hiking / Trekking',
                                url: 'javascript:;'
                            },
                            {
                                id: 152,
                                text: 'Casual / Travel',
                                url: 'javascript:;'
                            },
                            {
                                id: 153,
                                text: 'Ski / Snow',
                                url: 'javascript:;'
                            }
                        ]
                    },
                    {
                        id: 156,
                        text: 'Brands',
                        url: 'javascript:;',
                    },
                    {
                        id: 157,
                        text: 'Swim',
                        url: 'javascript:;',
                    },
                    {
                        id: 158,
                        text: 'Trends',
                        url: 'javascript:;',
                        items: [
                            {
                                id: 118,
                                text: 'Baselayers',
                                url: 'javascript:;'
                            },
                            {
                                id: 119,
                                text: 'Shirts & Tops',
                                url: 'javascript:;'
                            },
                            {
                                id: 120,
                                text: 'Pants',
                                url: 'javascript:;'
                            },
                            {
                                id: 121,
                                text: 'Shorts',
                                url: 'javascript:;'
                            },
                            {
                                id: 122,
                                text: 'Shop All',
                                url: 'javascript:;'
                            }
                        ]
                    }
                ]
            },
            {
                id: 3,
                text: 'Kids',
                url: 'javascript:;'
            },
            {
                id: 4,
                text: 'Activity',
                url: 'javascript:;',
                items: [
                    {
                        id: 12,
                        text: 'Footwear',
                        url: 'javascript:;',
                        items: [
                            {
                                id: 121,
                                text: 'Hiking',
                                url: 'javascript:;'
                            },
                            {
                                id: 122,
                                text: 'Winter',
                                url: 'javascript:;'
                            },
                            {
                                id: 123,
                                text: 'Casual',
                                url: 'javascript:;'
                            },
                            {
                                id: 124,
                                text: 'Sandals',
                                url: 'javascript:;'
                            },
                            {
                                id: 125,
                                text: 'Shop All',
                                url: 'javascript:;'
                            }
                        ]
                    },
                    {
                        id: 13,
                        text: 'Accessories',
                        url: 'javascript:;',
                        items: [
                            {
                                id: 131,
                                text: 'Backpacks & Bags',
                                url: 'javascript:;'
                            },
                            {
                                id: 132,
                                text: 'Headwear',
                                url: 'javascript:;'
                            },
                            {
                                id: 133,
                                text: 'Gloves',
                                url: 'javascript:;'
                            },
                            {
                                id: 134,
                                text: 'Shop All',
                                url: 'javascript:;'
                            }
                        ]
                    },
                ]
            },
            {
                id: 5,
                text: 'Sale',
                url: 'javascript:;',
                class: 'highlighted'
            }
        ]
    }
})
