# catchsmart-assignment

Es nezinu kādas tehnoloģijas jūs izmantojat, taču kā bāzi izmantoju `vue-cli` un pašu komponenti rakstīju SFC (Single-File Component) formātā. 
Par cik teicāt, ka komponentei jādarbojas pārceļot jaunā projektā, tad 3. puses bibliotēkas mēģināju neizmantot. 
Bet tā, būtu izmantojis `bootstrap` šī uzdevuma veikšanai un `vue-svg-inline-loader` ikonu importēšanai. 

*Datus glabāju izmantojot `vuex`, `src/store.js` failā.

*Komponentes atrašanās vieta `src/components/navigation`

Komponenti rakstīju vairākos failos, jo esmu tikai par pārskatāmu un strukturētu kodu.

Šo komponenti varētu pilnveidot daudz un dažādi, taču veidoju tikai tās lietas, kuras tika prasītas uzdevuma aprakstā.

## Projekta uzstādīšana
```
yarn install vai npm install
```

### Projekta palaišana
```
yarn run serve vai npm run serve
```

Atveram browswerī http://localhost:8080/
