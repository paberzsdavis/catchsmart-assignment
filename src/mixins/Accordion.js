let accordion = {
    methods: {
        $beforeEnter(e) {
            e.style.height = '0'

            setTimeout(() => {
                e.style.height = `${e.scrollHeight}px`
            }, 50) // a little delay for animation
        },

        $afterEnter(e) {
            e.style.height = 'auto'
        },

        $beforeLeave(e) {
            e.style.height = `${e.scrollHeight}px`

            setTimeout(() => {
                e.style.height = '0'
            }, 50) // a little delay for animation
        }
    }
}

export default accordion
